from django.apps import AppConfig


class DogbuddyMessagingConfig(AppConfig):
    name = 'dogbuddy_messaging'
