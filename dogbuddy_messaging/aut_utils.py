from django.contrib.auth.models import Permission


def add_permissions_for_contact_management_to_user(user):
    """
    adds creation, change and delete permissions for Contacts to user so that he can perform contact managment
    tasks in the admin section.
    :param user: CustomUser
    :return:
    """
    can_add_contact_permission = Permission.objects.get(name='Can add contact')
    can_change_contact_permission = Permission.objects.get(name='Can change contact')
    can_delete_contact_permission = Permission.objects.get(name='Can delete contact')

    user.user_permissions.add(can_add_contact_permission)
    user.user_permissions.add(can_change_contact_permission)
    user.user_permissions.add(can_delete_contact_permission)
    return







