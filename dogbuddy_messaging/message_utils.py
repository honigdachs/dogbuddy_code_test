from models import Contact
from models import UserConversationLink
from models import Conversation
from models import Message
from django.core.exceptions import PermissionDenied


def get_name_to_display_for_contact(email, current_user):
    """
    In the model of this application it is possible that a user receives a message from a user that is not in his
    contact list yet. In that case the email of the sending user will be shown as the contact name. Once the user
    adds a contact with the email that was not existing in the contact list yet, the contact name will be shown
    normally.

    This method checks if the email is already existing in the contact list of the current user, and if so
    returns the contact's name, otherwise it returns the email
    :param email:
    :param current_user:
    :return: string
    """
    existing_contact = Contact.objects.filter(contact_owner=current_user, email=email)
    if len(existing_contact) > 0:
        return existing_contact[0].name
    else:
        return email


def create_new_conversation_and_conversation_links(message_sender, message_receiver):
    """
    Creates a new conversation, conversation links for the sender and receiver and saves the objects.
    :param message_sender:
    :param message_receiver:
    :return : Conversation
    """
    new_conversation = Conversation()
    new_conversation.save()

    conversation_link_message_sender = UserConversationLink()
    conversation_link_message_sender.email = message_sender
    conversation_link_message_sender.conversation = new_conversation
    conversation_link_message_sender.save()

    conversation_link_message_receiver = UserConversationLink()
    conversation_link_message_receiver.email = message_receiver
    conversation_link_message_receiver.conversation = new_conversation
    conversation_link_message_receiver.save()
    return new_conversation


def get_or_create_conversation(message_sender, message_receiver):
    """
    Looks up if a conversation exists where both users participate. If it finds one it returns the conversation,
    otherwise creates a new conversation, creates new conversation links for both users and returns the newly
    created conversation
    :param message_sender: email
    :param message_receiver: email
    :return:
    """
    existing_conversation_links_for_message_sender = UserConversationLink.objects.filter(email=message_sender)
    existing_conversation_links_for_message_receiver = UserConversationLink.objects.filter(email=message_receiver)

    message_sender_conversations = [conversation.conversation_id for conversation in
                                    existing_conversation_links_for_message_sender]
    message_receiver_conversations = [conversation.conversation_id for conversation in
                                      existing_conversation_links_for_message_receiver]
    # now we check if a conversation exists where both users participate:
    matching_conversations = set(message_sender_conversations) & set(message_receiver_conversations)
    # there should never be more than one matching conversation between two users:
    if len(matching_conversations) > 1:
        raise ValueError('more than one matching conversation was found between two users. This should not happen.')
    if len(matching_conversations) == 1:
        matching_conversation_id = list(matching_conversations)[0]
        existing_conversation = Conversation.objects.get(id=matching_conversation_id)
        return existing_conversation
    if len(matching_conversations) == 0:
        new_conversation = create_new_conversation_and_conversation_links(message_sender, message_receiver)
        return new_conversation
    return None


def user_has_access_to_conversation(conversation, current_user):
    """
    checks if the user participates in the given conversation
    :param conversation: Conversation
    :param current_user: CustomUser
    :return:
    """
    needed_user_conversation_link = UserConversationLink.objects.filter(conversation=conversation,
                                                                        email=current_user.email)
    if len(needed_user_conversation_link) > 0:
        return True
    return False


def get_conversation_partner_email(conversation, current_user):
    """
    returns the conversation partner's email of a given conversation
    :param conversation: Conversation
    :param current_user: CustomUser
    :return:
    """
    all_user_conversation_links_of_conversation = UserConversationLink.objects.filter(conversation=conversation)
    for conversation in all_user_conversation_links_of_conversation:
        if conversation.email != current_user.email:
            return conversation.email


def conversation_has_unread_messages(message_list):
    """
    Checks if a conversation has unread messages
    :param message_list:
    :return:
    """
    for message in message_list:
        if not message.was_read:
            return True
    return False


def get_all_conversations_with_messages_for_user(current_user):
    """
    Returns a list of all conversation objcects that are linked to a user
    :param current_user: CustomUser
    :return: dict {
        messages: [Message, Message, ..],
        conversation_partner: 'name of the contact of the conversation',
        conversation_partner_email: 'email of the conversation partner,
        conversation_id : 'the id of the conversation'
    }
    """
    current_user_email = current_user.email
    all_conversations_with_messages = []
    conversations_with_unread_messages = []
    all_conversations_linked_to_user = UserConversationLink.objects.filter(email=current_user_email)

    for conversation_link in all_conversations_linked_to_user:
        conversation_id = conversation_link.conversation.id
        conversation_messages = Message.objects.filter(conversation=conversation_link.conversation)
        conversation_partner_email = get_conversation_partner_email(
                conversation_link.conversation, current_user)
        conversation_dict = {
            'messages': [message for message in conversation_messages],
            'conversation_partner': get_name_to_display_for_contact(conversation_partner_email, current_user),
            'conversation_partner_email': conversation_partner_email,
            'conversation_id': conversation_id
        }
        # we check if the conversation has unread messages, if so we will push it to the top for the
        # templates
        if conversation_has_unread_messages(conversation_messages):
            conversations_with_unread_messages.append(conversation_dict)
        else:
            all_conversations_with_messages.append(conversation_dict)

    # now we add the conversations that have unread messages to the top of the list and return it reversed
    # for the template order
    all_conversations_with_messages.extend(conversations_with_unread_messages)
    return all_conversations_with_messages[::-1]


def get_name_to_display_for_message(message_sender, current_user):
    """
    checks which name to display for a message. It could be Me the current user sent the message
    or the contact name of the message sender
    :param message_sender:
    :param current_user:
    :return:
    """
    if message_sender == current_user.email:
        return 'Me'
    return get_name_to_display_for_contact(message_sender, current_user)


def get_all_messages_for_conversation(conversation_id, current_user):
    """
    Returns all the messages for a given conversation with the right string for the message sender
    :param conversation_id:
    :param current_user:
    :raise PermissionDeniedException if a user tries to access a conversation that he is not participating in
    :return:
    """
    conversation = Conversation.objects.filter(id=conversation_id)
    if len(conversation) == 0:
        return
    if user_has_access_to_conversation(conversation, current_user):
        all_messages_of_conversation = []
        for message in Message.objects.filter(conversation=conversation):
            # since we load the whole conversation we mark all messages as read for the front-end
            message.was_read = True
            message.save()
            message_info = {
                'sender': get_name_to_display_for_message(message.from_email, current_user),
                'message_text': message.message_text
            }
            all_messages_of_conversation.append(message_info)

        # we inverse the list so that the newest messages appear on top
        return all_messages_of_conversation[::-1]

    else:
        raise PermissionDenied
