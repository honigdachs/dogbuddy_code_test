from __future__ import unicode_literals
import re
from django.conf import settings
from django.contrib.auth.models import (AbstractBaseUser, PermissionsMixin,
                                        UserManager)
from django.core.mail import send_mail
from django.core import validators
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.utils.http import urlquote

# Create your models here.


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """
    A custom user class that basically mirrors Django's `AbstractUser` class
    and doesn't force `first_name` or `last_name` with sensibilities for
    international names.

    http://www.w3.org/International/questions/qa-personal-names
    """
    username = models.CharField(_('username'), max_length=30, unique=True,
                                help_text=_('Required. 30 characters or fewer. Letters, numbers and '
                                            '@/./+/-/_ characters'),
                                validators=[
        validators.RegexValidator(re.compile(
            '^[\w.@+-]+$'), _('Enter a valid username.'), 'invalid')
    ])
    full_name = models.CharField(_('full name'), max_length=254, blank=True)
    short_name = models.CharField(_('short name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), max_length=254, unique=True)
    is_staff = models.BooleanField(_('staff status'), default=True,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __unicode__(self):
        return self.username

    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.username)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = self.full_name
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.short_name.strip()

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])


class Contact(models.Model):

    name = models.CharField(max_length=200)
    email = models.EmailField(_('email address'), max_length=254)
    contact_owner = models.ForeignKey(settings.AUTH_USER_MODEL)


class Conversation(models.Model):
    created_at = models.DateTimeField(default=timezone.now)


class UserConversationLink(models.Model):
    """
    this class links a user to a conversation. I have decided to make the user's email the primary key
    for the user. --> A user is linked to a conversation through his email. I do not link the user to the
    conversation through the models.ForeignKey(settings.AUTH_USER_MODEL) because the user I am writing the
    message to may not be an actual user yet. He might sign up after the message was sent to him with
    the email that is saved here. This way conversations can be assigned to users that were created before
    the user became an actual user of the platform.
    """
    email = models.EmailField(_('email address'), max_length=254)
    conversation = models.ForeignKey(Conversation)


class Message(models.Model):
    conversation = models.ForeignKey(Conversation)
    from_email = models.EmailField(max_length=254)
    to_email = models.EmailField(max_length=254)
    message_text = models.TextField(blank=True, max_length=1500)
    was_read = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)
