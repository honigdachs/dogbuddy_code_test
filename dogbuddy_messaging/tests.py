from django.test import TestCase
from django.test import Client
from dogbuddy_messaging.models import CustomUser
from dogbuddy_messaging.models import Contact
from dogbuddy_messaging.models import UserConversationLink
# Create your tests here.


class AuthTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.test_user_name = 'djangotestcase'
        self.test_user_email = 'django@testacase.com'
        self.test_user_password = 'secret'
        self.user_for_test = self.create_user(self.test_user_name, self.test_user_password,
                                              self.test_user_email)

    def create_user(self, username, password, email, is_superuser=True):
        """
        creates a user for the test cases
        :param username:
        :param password:
        :param email:
        :param is_superuser:
        :return:
        """
        user = CustomUser.objects.create_user(username=username, password=password, email=email)
        user.is_superuser = is_superuser
        user.save()
        return user

    def create_contact_for_test_user(self, name, email):
        contact_owning_user = self.user_for_test
        new_contact = Contact(name=name, email=email,
                              contact_owner=contact_owning_user)
        new_contact.save()
        return new_contact

    def test_register_page_available(self):
        response = self.client.get("/register/")
        self.assertContains(response, 'Sign Up')

    def test_login_page_available(self):
        response = self.client.get("/login/")
        self.assertContains(response, 'Please Login')

    def test_user_creation_and_login(self):
        logged_in = self.client.login(username=self.test_user_email, password=self.test_user_password)
        self.assertTrue(logged_in)

    def test_messages_page_available(self):
        self.client.login(username=self.test_user_email, password=self.test_user_password)
        response = self.client.get("/")
        self.assertContains(response, 'Messages')

    def test_contact_creation(self):

        self.create_contact_for_test_user('newContactForTest', 'awesome@testuser.com')
        # now we go to the 'friends page and assert that the contact exists there'
        self.client.login(username=self.test_user_email, password=self.test_user_password)

        response = self.client.get("/friends/")
        self.assertContains(response, 'newContactForTest')
        self.assertContains(response, 'awesome@testuser.com')

    def test_message_creation(self):
        self.create_contact_for_test_user('messageTestContact', 'message@messagetest.com')
        self.client.login(username=self.test_user_email, password=self.test_user_password)
        # we create a new message:
        # the test message text should not be to long because jquery cuts the message text
        # if it is and that would break the test.
        test_message_text = 'Save this message and show it on the messages page'
        response = self.client.post('/send_message/', {'from_email': self.test_user_email,
                                                       'to_email': 'test@testreceiver.com',
                                                       'message_text': test_message_text})

        # after we send the message we get redircted to the messages page:
        self.assertEquals('/', response.url)
        messages_page = self.client.get("/")
        self.assertContains(messages_page, test_message_text)
        self.assertContains(messages_page, 'test@testreceiver.com')

    def test_conversation_page(self):
        self.create_contact_for_test_user('conversationTestContact', 'conversation@conversationtest.com')
        self.client.login(username=self.test_user_email, password=self.test_user_password)
        # we create a new message:
        # the test message text should not be to long because jquery cuts the message text
        # if it is and that would break the test.
        test_message_text = 'Save this message and show it to me in the conversation page'
        response = self.client.post('/send_message/', {'from_email': self.test_user_email,
                                                       'to_email': 'test@testreceiver.com',
                                                       'message_text': test_message_text})

        conversation_link_for_message = UserConversationLink.objects.filter(email=self.test_user_email)
        self.assertIsNotNone(conversation_link_for_message)
        conversation_to_check = conversation_link_for_message[0].conversation

        conversation_id = conversation_to_check.id
        # now we go to the corresponding conversation page and assure that the message shows up there:
        conversation_url = '/conversation/' + str(conversation_id) + '/'
        conversation_page = self.client.get(conversation_url)
        self.assertContains(conversation_page, test_message_text)



