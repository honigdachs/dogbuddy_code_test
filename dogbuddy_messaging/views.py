from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from forms import CustomUserCreationForm
from django.template import RequestContext
from django.views.decorators.http import require_http_methods
from django.core.exceptions import PermissionDenied
from message_utils import get_name_to_display_for_contact
from message_utils import get_or_create_conversation
from message_utils import get_all_conversations_with_messages_for_user
from message_utils import get_conversation_partner_email
from message_utils import get_all_messages_for_conversation
from forms import MessageForm
from dogbuddy_messaging.models import Contact
from dogbuddy_messaging.models import Conversation

from aut_utils import add_permissions_for_contact_management_to_user
# Create your views here.


def register_user(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)  # create form object
        if form.is_valid():
            form.save()
            new_user = authenticate(username=form.cleaned_data['email'],
                                    password=form.cleaned_data['password1'],
                                    )
            login(request, new_user)
            add_permissions_for_contact_management_to_user(new_user)
            return HttpResponseRedirect('/')
    else:
        form = CustomUserCreationForm()
    return render_to_response('register.html', {'form': form}, context_instance=RequestContext(request))


@login_required(login_url='login/')
def messages(request):
    all_conversations_with_messages = get_all_conversations_with_messages_for_user(request.user)
    return render_to_response('messages.html', {'conversations_with_messages': all_conversations_with_messages},
                              context_instance=RequestContext(request))


@login_required(login_url='login/')
def friends(request):
    current_user = request.user
    contacts_belonging_to_user = Contact.objects.filter(contact_owner=current_user).order_by('name')
    return render_to_response('friends.html', {
        'contacts': contacts_belonging_to_user,
    }, context_instance=RequestContext(request))


@login_required(login_url='login/')
@require_http_methods(['POST'])
def write_message(request):
    message_receiver = request.POST.get("receiver_email", "")
    if message_receiver:
        message_sender = request.user.email
        contact_name = get_name_to_display_for_contact(message_receiver, request.user)
        message_form = MessageForm(initial={'from_email': message_sender, 'to_email': message_receiver})

        return render_to_response('new_message.html', {
            'message_form': message_form,
            'message_sender': message_sender,
            'message_receiver': message_receiver,
            'contact_name': contact_name
        }, context_instance=RequestContext(request))

    else:
        # the message_receiver must be always set in the post form.
        raise PermissionDenied


@login_required(login_url='login/')
@require_http_methods(['POST'])
def send_message(request):
    submitted_form = MessageForm(request.POST)  # create form object
    if submitted_form.is_valid():
        message_sender = submitted_form.cleaned_data['from_email']
        message_receiver = submitted_form.cleaned_data['to_email']

        conversation_to_extend = get_or_create_conversation(message_sender, message_receiver)
        if not conversation_to_extend:
            raise ValueError("Something went wrong during the retrieval or creation of a new conversation")
        new_message = submitted_form.save(commit=False)
        # we do not save the message yet first we find out which conversation the message belongs to
        new_message.conversation = conversation_to_extend
        new_message.save()

    return HttpResponseRedirect('/')


@login_required(login_url='login/')
def show_conversation(request, conversation_id):
    conversation_to_show = Conversation.objects.filter(id=conversation_id)
    current_user = request.user
    all_messages = get_all_messages_for_conversation(conversation_id, current_user)
    conversation_partner_name = get_name_to_display_for_contact(
        get_conversation_partner_email(conversation_to_show, current_user), current_user
    )

    return render_to_response('conversation.html', {
        'all_messages': all_messages,
        'conversation_partner': conversation_partner_name
    }, context_instance=RequestContext(request))

