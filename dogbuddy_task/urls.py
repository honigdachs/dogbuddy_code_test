from django.conf.urls import include, url
from django.contrib.auth import views
from dogbuddy_messaging.forms import LoginForm
from dogbuddy_messaging import views as dogbuddy_messaging_views
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


urlpatterns = [
    # Examples:
    # url(r'^$', 'django_custom_user_example.views.home', name='home'),
    # url(r'^django_custom_user_example/',
    # include('django_custom_user_example.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/',
    # include('django.contrib.admindocs.urls')),
    url(r'^$', dogbuddy_messaging_views.messages, name='messages'),
    url(r'^friends/$', dogbuddy_messaging_views.friends, name='friends'),
    url(r'^write_message/$', dogbuddy_messaging_views.write_message, name='write_message'),
    url(r'^send_message/$', dogbuddy_messaging_views.send_message, name='send_message'),
    url(r'^conversation/(?P<conversation_id>\d+)/$', dogbuddy_messaging_views.show_conversation, name='show_conversation'),

    url(r'^login/$', views.login, {'template_name': 'login.html', 'authentication_form': LoginForm}),
    url(r'^register/$', dogbuddy_messaging_views.register_user, name='register'),
    url(r'^logout/$', views.logout, {'next_page': '/login'}),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
]
