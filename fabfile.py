from fabric.api import local


def prepare_deployment():
    local('python manage.py test')


def deploy():
    pass
    # deployment strategy script would be placed here empty for now since the target server environment is
    # unknown.
