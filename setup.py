from setuptools import setup, find_packages

setup(
    name="dogbuddy_task",
    version="1.0.0",
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "Django==1.9",
        "Fabric==1.12.0",
    ],

)
