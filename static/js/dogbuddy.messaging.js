/**
 * Created by matyas on 20/08/16.
 */

var getUrlToActivate = function (urlPartList) {

    if ($.inArray('write_message', urlPartList) > -1) {
        return '/friends/'
    }
    if ($.inArray('friends', urlPartList) > -1) {
        return '/friends/'
    }
    if ($.inArray('friends', urlPartList) > -1) {
        return '/'
    }
    return '/';
};


$(document).ready(function () {

    // <LINK SHRINKING >
      var backToMessagesLink = $('.messages-back-link');
        var width = $(window).width();
        // changing the back link to the short version on mobile devices
        if(width < 700){
            backToMessagesLink.text('<<');
        }
    // </LINK SHRINKING>

    // <MESSAGE PREVIEW SHRINKING > //

    // if the message text is to long to preview on the messages page we cut it the message and
    // show only a part of it



    $('.centered-message-link').each(function(){
        console.log($(this).text().length);
        if($(this).text().length > 100){
           // $(this).text('I will cut you');
            var shortenedMessage = $(this).text().substring(0,50);
            var stringToShow = shortenedMessage + '....';
            $(this).text(stringToShow);
            console.log($(this).text().substring(0,100))
        }
});



    // </MESSAGE PREVIEW SHRINKING> //


    // <NAVBAR URL COLORING>

    var currentUrl = window.location.href;
    var urlParts = currentUrl.split('/');

    // We check which url we have to activate and do it-
    $('#base-navbar > li').each(function () {
        var linkAdress = $(this).children('a').attr('href')
        // we remove all active links.
        $(this).children('a').removeClass('base-active');
        if (linkAdress == getUrlToActivate(urlParts)) {
            console.log('log adding class...');
            // and set the right one to active
            $(this).children('a').addClass('base-active')
        }

    });
     // </ NAVBAR URL COLORING>
});